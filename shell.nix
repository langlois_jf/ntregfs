with import <nixpkgs> {}; {
  ntregfsEnv = stdenv.mkDerivation {
    name = "ntregfs";
    buildInputs = [ stdenv autoconf automake fuse hivex pkgconfig ];
  };
}
