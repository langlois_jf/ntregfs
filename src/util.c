#define _GNU_SOURCE

#include "util.h"

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include <hivex.h>


hive_node_h path_to_node(hive_h * const handle, char const * const path) {
	char * const path_copy = strdup(path);
	char * saveptr = path_copy;
	char const * path_segment;
	hive_node_h result = hivex_root(handle);
	if(result == 0) {
		int unsigned hxerr = errno;
		fprintf(stderr, "Could not get root node. (error code: %i)\n", hxerr);
	} else {
		path_segment = strtok_r(path_copy, "/", &saveptr);
		while(path_segment != NULL) {
			result = hivex_node_get_child (handle, result, path_segment);
			if(result == 0) {
				int const hxerr = errno;
				fprintf(stderr, "Could not get child node named \"%s\". (error code: %i)\n", path_segment, hxerr);
				return 0;
			} else {
				path_segment = strtok_r(NULL, "/", &saveptr);
			}
		}
	}
	free(path_copy);
	return result;
}


hive_value_h path_to_value(hive_h * const handle, char const * const path) {
	char const * const last_separator = strrchr(path, '/');
	size_t const node_path_len = last_separator - path;
	char * const node_path = malloc(node_path_len + 1);
	char const * const value_key = strdup(last_separator + 1);
	hive_node_h value_parent;
	memcpy(node_path, path, node_path_len);
	node_path[node_path_len] = '\0';

	value_parent = path_to_node(handle, node_path);
	if(value_parent == 0) {
		return 0;
	} else {
		hive_value_h result = hivex_node_get_value(handle, value_parent, value_key);
		if(result == 0) {
			int unsigned hxerr = errno;
			fprintf(stderr, "Could not get value with key \"%s\". (error code: %i)\n", path, hxerr);
		}
		return result;
	}
}
