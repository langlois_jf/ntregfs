#define _GNU_SOURCE
#include "../operations.h"
#include "../shared_state.h"
#include "../util.h"

#include <stdio.h>
#include <string.h>

#include <hivex.h>


int ntregfs_read(char const * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
	hive_type value_type;
	size_t value_len;
	char const * value_value;
	hive_value_h const value = path_to_value(ntregfs_hive_handle, path);
	(void) fi;

	if(value == 0) {
		return -ENOENT;
	} else {
		if((value_value = hivex_value_value(ntregfs_hive_handle, value, &value_type, &value_len)) == 0) {
			int unsigned hxerr = errno;
			fprintf(stderr, "Could not get the type and size of value \"%s\". (error code: %i)\n", path, hxerr);
			return -1;
		} else {
			if(offset < (off_t) value_len) {
				if(offset + size > value_len) {
					size = value_len - offset;
				}
				memcpy(buf, value_value + offset, size);
			} else {
				size = 0;
			}
			free((char * const) value_value);
			return size;
		}
	}
}
