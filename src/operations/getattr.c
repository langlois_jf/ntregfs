#define _GNU_SOURCE

#include "../operations.h"
#include "../shared_state.h"
#include "../util.h"

#include <errno.h>
#include <sys/stat.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include <hivex.h>


int ntregfs_getattr(char const * path, struct stat * stbuf) {
	hive_node_h const node_for_path = path_to_node(ntregfs_hive_handle, path);
	if(node_for_path == 0) {
		hive_value_h const value_for_path = path_to_value(ntregfs_hive_handle, path);
		if(value_for_path == 0) {
			return -ENOENT;
		} else {
			hive_type value_type;
			size_t value_len;
			if(hivex_value_type(ntregfs_hive_handle, value_for_path, &value_type, &value_len) == -1) {
				int unsigned hxerr = errno;
				fprintf(stderr, "Could not get value type and length for key \"%s\". (error code: %i)\n", path, hxerr);
				return -1;
			} else {
				stbuf->st_mode = S_IFREG | 0444;
				stbuf->st_nlink = 1;
				stbuf->st_size = value_len;
				return 0;
			}
		}
	} else {
		stbuf->st_mode = S_IFDIR | 0755;
		stbuf->st_nlink = 2;
		return 0;
	}
}
