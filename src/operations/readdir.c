#define _GNU_SOURCE
#include "../operations.h"
#include "../shared_state.h"
#include "../util.h"

#include <stdio.h>


int ntregfs_readdir(char const * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {
	hive_node_h const node_for_path = path_to_node(ntregfs_hive_handle, path);
	(void) offset;
	(void) fi;

	if(node_for_path == 0) {
		return -ENOENT;
	} else {
		hive_node_h const * const children = hivex_node_children(ntregfs_hive_handle, node_for_path);
		hive_value_h const * const values = hivex_node_values(ntregfs_hive_handle, node_for_path);

		if(children == 0) {
			int const hxerr = errno;
			fprintf(stderr, "Could not read registry node children. (error code: %i)\n", hxerr);
			return -1; /* TODO better error code? */
		} else {
			hive_node_h const * idx = children;
			filler(buf, ".", NULL, 0);
			filler(buf, "..", NULL, 0);
			for(; *idx != (hive_node_h) NULL; ++idx) {
				char const * const node_name = hivex_node_name(ntregfs_hive_handle, *idx);
				if(node_name == NULL) {
					int const hxerr = errno;
					fprintf(stderr, "Could not retrieve node name. (error code: %i)\n", hxerr);
					return -1;
				} else {
					filler(buf, node_name, NULL, 0);
					free((char * const) node_name);
				}
			}
			free((hive_node_h * const) children);
		}

		if(values == NULL) {
			int unsigned hxerr = errno;
			fprintf(stderr, "Could not get values for node \"%s\". (error code: %i)\n", path, hxerr);
			return -1;
		} else {
			hive_value_h const * idx = values;
			for(; *idx != (hive_value_h) NULL; ++idx) {
				char const * const value_key = hivex_value_key(ntregfs_hive_handle, *idx);
				if(value_key == NULL) {
					int const hxerr = errno;
					fprintf(stderr, "Could not retrieve value key. (error code: %i)\n", hxerr);
					return -1;
				} else {
					filler(buf, value_key, NULL, 0);
					free((char * const) value_key);
				}
			}
			free((hive_value_h * const) values);
		}

		return 0;
	}
}
