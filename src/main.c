#define _GNU_SOURCE

#include "operations.h"
#include "shared_state.h"
#include "util.h"

#include <errno.h>
#include <sys/stat.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define FUSE_USE_VERSION 26
#include <fuse.h>

#include <hivex.h>


struct options_t {
	char const * hive_path;
	char const * ro;
} options;

/** macro to define options */
#define NTREGFS_OPT_KEY(t, p, v) { t, offsetof(struct options_t, p), v }

static struct fuse_opt ntregfs_options[] = {
	NTREGFS_OPT_KEY("ro", ro, 0),
	FUSE_OPT_END
};
hive_h * ntregfs_hive_handle;


static int ntregfs_opt_proc(void * data, char const * arg, int key, struct fuse_args * outargs) {
	struct options_t * odata = data;
	(void) outargs;
	if(key == FUSE_OPT_KEY_NONOPT && odata->hive_path == NULL) {
		odata->hive_path = arg;
		return 0;
	} else {
		return 1;
	}
}

static struct fuse_operations ntregfs_operations = {
	.getattr = ntregfs_getattr,
	.readdir = ntregfs_readdir,
	.open = ntregfs_open,
	.read = ntregfs_read
};

int main(int argc, char * argv[]) {
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	memset(&options, 0, sizeof(struct options_t));
	if(fuse_opt_parse(&args, &options, ntregfs_options, &ntregfs_opt_proc) == -1) {
		return -1;
	} else {
		if(options.hive_path == NULL) {
			fprintf(stderr, "A hive file path was not supplied.\n");
			return -1;
		} else {
			ntregfs_hive_handle = hivex_open(options.hive_path, 0);
			if(ntregfs_hive_handle == NULL) {
				int const hxerr = errno;
				fprintf(stderr, "Could not open hive file: \"%s\" (errno: %i)\n", options.hive_path, hxerr);
				fuse_opt_free_args(&args);
				return -1;
			} else {
				int const result = fuse_main(args.argc, args.argv, &ntregfs_operations, NULL);
				hivex_close(ntregfs_hive_handle);
				fuse_opt_free_args(&args);
				return result;
			}
		}
	}
}
