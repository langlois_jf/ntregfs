#ifndef OPERATIONS_H
#define OPERATIONS_H

#define FUSE_USE_VERSION 26
#include <fuse.h>

int ntregfs_getattr(char const * path, struct stat * stbuf);
int ntregfs_readdir(char const * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi);
int ntregfs_open(char const * path, struct fuse_file_info * fi);
int ntregfs_read(char const * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi);

#endif
