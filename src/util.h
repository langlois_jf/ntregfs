#ifndef UTIL_H
#define UTIL_H

#include <hivex.h>

hive_node_h path_to_node(hive_h * const handle, char const * const path);
hive_value_h path_to_value(hive_h * const handle, char const * const path);

#endif /* #define UTIL_H */
